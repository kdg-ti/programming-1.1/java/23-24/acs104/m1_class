package be.kdg;

import java.util.Scanner;
public class Ex10_Calculate {
    public static void main(String[] args) {
        //declarations:
        double number1;
        double number2;
        int choice;
        double result;
        Scanner keyboard = new Scanner(System.in);

        //input:
        System.out.print("Enter a number: ");
        number1 = keyboard.nextDouble();
        System.out.print("Enter another number: ");
        number2 = keyboard.nextDouble();
        System.out.println("Choose an operation:\n" +
                "\t1 add\n" +
                "\t2 subtract\n" +
                "\t3 multiply\n" +
                "\t4 divide");
        System.out.print("Your choice: ");
        choice = keyboard.nextInt();

        //calculate and output:
        if(choice == 1) { //add
            result = number1 + number2;
            System.out.println(number1 + " + " + number2 + " = " + result);
        }
        if(choice == 2) { //subtract
            result = number1 - number2;
            System.out.println(number1 + " - " + number2 + " = " + result);
        }
        if(choice == 3) { //product
            result = number1 * number2;
            System.out.println(number1 + " * " + number2 + " = " + result);
        }
        if(choice == 4) { //divide
            result = number1 / number2;
            System.out.println(number1 + " / " + number2 + " = " + result);
        }
    }
}