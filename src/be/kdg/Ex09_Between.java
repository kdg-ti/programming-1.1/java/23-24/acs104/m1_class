package be.kdg;

import java.util.Scanner;
public class Ex09_Between {
    public static void main(String[] args) {
        int num1;
        int num2;
        int num3;
        int middle = 0;
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter first number: ");
        num1 = keyboard.nextInt();
        System.out.print("Enter second number: ");
        num2 = keyboard.nextInt();
        System.out.print("Enter third number: ");
        num3 = keyboard.nextInt();

        // Approach 1:
        if (((num2 <= num1) && (num1 <= num3)) || ((num3 <= num1) && (num1 <= num2))) {
            // num1 between num2 and num3
            middle = num1;
        }
        if (((num1 <= num2) && (num2 <= num3)) || ((num3 <= num2) && (num2 <= num1))) {
            // num2 between num1 and num3
            middle = num2;
        }
        if (((num1 <= num3) && (num3 <= num2)) || ((num2 <= num3) && (num3 <= num1))) {
            // num3 between num1 and num2
            middle = num3;
        }
        System.out.println("(1) Middle number is " + middle);

        // Approach 2:
        middle = num1;
        if (((num1 <= num2) && (num2 <= num3)) || ((num3 <= num2) && (num2 <= num1))) {
            // num2 between num1 and num3
            middle = num2;
        } else if (((num1 <= num3) && (num3 <= num2)) || ((num2 <= num3) && (num3 <= num1))) {
            // num3 between num1 and num2
            middle = num3;
        }

        System.out.println("(2) Middle number is " + middle);
    }
}
