package be.kdg;

import java.util.Scanner;

public class Ex07_BMIv1 {
    public static void main(String[] args) {
        int weight;
        double length;
        double bmi;
        Scanner keyboard = new Scanner(System.in);

        System.out.print("Dear patient, this program will calculate your BMI.\n");
        System.out.print("Enter your weight in kilograms: ");
        weight = keyboard.nextInt();

        System.out.print("Enter your length in meters: ");
        length = keyboard.nextDouble();

        bmi = weight / (length * length);

        // Initial version
        if (bmi < 18) {
            System.out.println("BMI " + bmi + " is underweight");
        }
        if (18 <= bmi && bmi < 25) {
            System.out.println("BMI " + bmi + " is healthy weight");
        }
        if (25 <= bmi && bmi < 30) {
            System.out.println("BMI " + bmi + " is overweight");
        }
        if (30 < bmi) {
            System.out.println("BMI " + bmi + " is obese");
        }

        // Improved version
        if (bmi < 18) {
            System.out.println("BMI " + bmi + " is underweight");
        } else if (bmi < 25) {
            System.out.println("BMI " + bmi + " is healthy weight");
        } else if (bmi < 30) {
            System.out.println("BMI " + bmi + " is overweight");
        } else {
            System.out.println("BMI " + bmi + " is obese");
        }
    }
}
